#!/bin/bash

# finds all files that end with .tf and formats them
find . -name "*.tf" -not -path "*/.terraform/*" -printf '%p' -exec bash -c 'echo "{}"; terraform fmt "{}"' \;