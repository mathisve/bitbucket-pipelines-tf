provider "aws" {
  region = "eu-central-1"
}

resource "aws_s3_bucket" "b" {
  bucket = "this-is-a-test-bucket-mathis"
  acl    = "public-read"
  tags   = var.tags
}