variable "tags" {
  type = map(string)

  default = {
    Name        = "this-is-a-test-bucket-mathis"
    Environment = "dev"
    Project     = "devops"
  }
}