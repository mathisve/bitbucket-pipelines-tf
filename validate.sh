export rootDir=$(pwd)
for dir in module/*/; do
    cd $rootDir/$dir
    echo "validating $dir"
    terraform init 
    terraform validate 
done